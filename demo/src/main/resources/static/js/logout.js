//Export function to be able to use in other js files with import.
export default function logout(){
    let idAdmin = localStorage.getItem("idAdmin");
    let data = {idAdmin:idAdmin}
    //Ajax request
    $.ajax({
        method: "POST",
        url: "/rest/logu/logout",
        data: data,
        async: true
    }).done(function (data) {
            var result = '';
            result = JSON.parse(data);
        if (result.exception != null) {
            Swal.fire("Error en el sistema. Por favor contacte al administrador.",
                result.exception,
                "error");
        }
        else {
            Swal.fire("Cerrando sesión",
                "",
                "success");
                setTimeout(function(){
                    window.location.href = "index.html";}, 2000);
        }
    });
}