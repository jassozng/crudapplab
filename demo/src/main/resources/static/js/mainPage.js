//This import is for logout function, that makes the logout icon works.
import logout from './logout.js';

//This function detects when any button on main page is pressed, then executes the corresponding function.
window.onload = function () {
    setData();
    $("#logoutTag").on('click', logout);
    $("#welcome").html("Bienvenido(a), "+localStorage.getItem("nombre")+".");
}

export default function setData(){
    let userData = localStorage.getItem("nombre") + "\n" + localStorage.getItem("area");
    let photo = localStorage.getItem("foto");
    let imgComponent = document.getElementById("userImg");
    imgComponent.src="";
    imgComponent.src="data:image/png;base64,"+photo;
    $("#userInfo").html(userData);
    //$("#userArea").html(area);
}