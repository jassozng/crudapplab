//This import needed. Like logout function and set data for topbar.
import logout from './logout.js';
import setData from './mainPage.js';
import cleanFields from './adminFunctions/addAdmin.js';

//This function detects when any of the buttons on the administrator page is pressed, then executes the corresponding function.
window.onload = function () {
    setData();
    getAll();
    let searchButton = document.getElementById("searchButton");
    searchButton.addEventListener("click", searchAdmin);
    let filterButton = document.getElementById("filterButton");
    filterButton.addEventListener("click", filterMenu);
    let downloadButton = document.getElementById("downloadButton");
    downloadButton.addEventListener("click", downloadRegister);
    $("#logoutTag").on('click', logout);
}

//Function to search an Admin.
function searchAdmin() {
    //Get the admin's name, token, Admin's id (Log) and filters values.
    let idAdminLog =  localStorage.getItem("idAdmin");
    let token = localStorage.getItem("token");
    let name = document.getElementById("txtSearch").value;
    let filterArea = document.getElementById("filterArea").value;
    let filterEstatus = document.getElementById("filterEstatus").value;
    //Check if filters have a default value.
    if(filterArea == "Tipo de área" && filterEstatus == "Estatus" || filterEstatus == undefined && filterArea == undefined){
        filterArea = "";
        filterEstatus = "";
    }else if(filterEstatus == "Estatus" || filterEstatus == undefined){
        filterEstatus = "";
    }
    else if(filterArea == "Tipo de área" || filterArea == undefined){
        filterArea = "";
    }
    //Declare data json for Ajax request.
    let data = {
        idAdminLog:idAdminLog,
        token:token,
        name: name,
        filterArea:filterArea,
        filterEstatus:filterEstatus
    }
    //Ajax request.
    $.ajax({
        method: "GET",
        url: "/rest/admin/search",
        data: data,
        async: true
    }).done(function (data) {
        let response = '';
        response = JSON.parse(data);
        //If there is any exception, this alert will show it.
        if (response.exception != null) {
            if(response.exception == "No data"){
                Swal.fire("Información",
                "No existe ningún registro con esos parámetros.",
                "info");
            }else{
                Swal.fire("La consulta no se pudo procesar",
                response.exception,
                "error");
            }
        }
        //If not...
        else {
            let records = [];
            records = response;
            $("#adminTable > tbody").empty();
            for(let i=0; i<records.length; i++){
                $('#adminTable > tbody').append(
                '<tr>'+
                '<td class="font-weight-bold"><input type="checkbox">&emsp; <img src="data:image/png;base64,'+records[i].photo+'" width="40" style="border-radius: 50%;">'+'&nbsp;'+records[i].name +" "+records[i].surname+'</td>'+
                '<td>'+records[i].area+'</td>'+
                '<td>'+records[i].email+'</td>'+
                '<td>'+records[i].estatus+'</td>'+
                '<td><button title="Editar administrador" class="btn btn-light" id="updateAdmin'+[i]+'"><span class="iconify" data-icon="bx:bx-edit" style="color:#3cc9c5" data-width="25"></span></button>'+'&nbsp;'+
                '<button type="button" title="Eliminar Administrador" class="btn btn-light" id="deleteAdmin'+[i]+'"><span class="iconify" data-icon="bx:bx-trash" style="color:red" data-width="25"></span></button>'+'&nbsp;'+
                '<button type="button" title="Ver perfil" class="btn btn-light" id="viewAdminProfile'+[i]+'"><span class="iconify" data-icon="akar-icons:eye" data-width="25"></span></button>'+
                '</tr>');
            }
        }
    });
}

//Function that brings all the administrator records.
function getAll(){
    //Get data for token validation.
    let token = localStorage.getItem("token");
    let idAdminLog = localStorage.getItem("idAdmin");
    //Declare data json for Ajax request.
    let data = {
        token:token,
        idAdminLog:idAdminLog
    }
    //Ajax request.
    $.ajax({
        type:"GET",
        url: "/rest/admin/getAll",
        async:true,
        data:data
    }).done(function(data){
            var result = '';
            result = JSON.parse(data);
            //If there's an exception...
            if(result.exception != null){
                //Check if exception is no data in db.
                if(result.exception == "No data"){
                    Swal.fire("La consulta no se pudo procesar",
                    "No existen registros de administradores en la base de datos",
                    "error");
                }
                //Other kind exception.
                else{
                    Swal.fire("La consulta no se pudo procesar",
                    result.exception,
                    "error");
                }
            }
            //If everything is fine..
            else{
                let records = [];
                records = result;
                for(let i=0; i<records.length; i++){
                    $('#adminTable > tbody').append('<tr>'+
                    '<td class="font-weight-bold"><input type="checkbox">&emsp; <img src="data:image/png;base64,'+records[i].photo+'" width="40" style="border-radius: 50%;">'+'&nbsp;'+records[i].name +" "+records[i].surname+'</td>'+
                    '<td>'+records[i].area+'</td>'+
                    '<td>'+records[i].email+'</td>'+
                    '<td>'+records[i].estatus+'</td>'+
                    '<td><button title="Editar administrador" class="btn btn-light" id="updateAdmin'+[i]+'"><span class="iconify" data-icon="bx:bx-edit" style="color:#3cc9c5" data-width="25"></span></button>'+'&nbsp;'+
                    '<button type="button" title="Eliminar Administrador" class="btn btn-light" id="deleteAdmin'+[i]+'"><span class="iconify" data-icon="bx:bx-trash" style="color:red" data-width="25"></span></button>'+'&nbsp;'+
                    '<button type="button" title="Ver perfil" class="btn btn-light" id="viewAdminProfile'+[i]+'"><span class="iconify" data-icon="akar-icons:eye" data-width="25"></span></button>'+
                    '</tr>');
                    $('#viewAdminProfile'+i+'').click(function(){
                        viewProfile(records[i]);
                    });
                    $('#deleteAdmin'+i+'').click(function(){
                        deleteAdmin(records[i].idAdmin);
                    });
                    $('#updateAdmin'+i+'').click(function(){
                        updateAdmin(records[i]);
                    });
                }
            }
        });
}

function viewProfile(data) {
    document.getElementById("content-container").innerHTML = "";
    $("#content-container").load("profileViewer.html",
    function (responseText, textStatus, XMLHttpRequest) {
        if (textStatus == "success") {
            let imgComponent = document.getElementById("userPhoto");
            imgComponent.src="";
            imgComponent.src="data:image/png;base64,"+data.photo;
            document.getElementById("userName").innerHTML = data.name + " " + data.surname;
            document.getElementById("pName").innerHTML = data.name + " " + data.surname;
            document.getElementById("pSurname").innerHTML = data.surname;
            document.getElementById("pEmail").innerHTML = data.email;
            document.getElementById("pArea").innerHTML = data.area;
            document.getElementById("pEstatus").innerHTML = data.estatus;
            $('#updateAdmin').click(function(){
                updateAdmin(data);
            });
        }
    });
}

function filterMenu() {
    Swal.fire({
        title: '<p class="font-weight-bold">Filtros de búsqueda</p>',
        html:
          '<select id="lstAreas" style="margin:0 auto;">'+
          '<option hidden selected>Tipo de área</option>'+
          '<option value="DDS">Desarrollo de software</option>'+
          '<option value="RH">Recursos Humanos</option>'+
          '<option value="VT">Ventas</option>'+
          '</select><br>'+
          '<select id="lstEstatus" style="margin:0 auto;">'+
          '<option hidden selected>Estatus</option>'+
          '<option value="1">Activo</option>'+
          '<option value="0">Inactivo</option>'+
          '</select><br></br>'+
          '<button type="button" class="btn btn-light" id="cleanFilters">Limpiar</button>'+
          '<button type="button" class="btn btn-info" id="setFilters">Aplicar Filtros</button>',
        showCloseButton: true,
        showConfirmButton: false,
        showCancelButton: false,
    });
    $("#setFilters").click(function(){
        document.getElementById("filterArea").value = $('#lstAreas :selected').text();
        document.getElementById("filterEstatus").value = $('#lstEstatus :selected').text();
        swal.close();
    });
    $("#cleanFilters").click(function(){
        $('#lstAreas').val("Tipo de área");
        $('#lstEstatus').val("Estatus");
    });
}

function downloadRegister() {
    alert("Hello 3");
}

function updateAdmin(data) {
    document.getElementById("content-container").innerHTML = "";
    $("#content-container").load("updateAdmin.html", 
    function (responseText, textStatus, XMLHttpRequest) {
        if (textStatus == "success") {
            let imgComponent = document.getElementById("userPhoto");
            imgComponent.src="";
            imgComponent.src="data:image/png;base64,"+data.photo;
            $('#name').val(data.name);
            $('#surname').val(data.surname);
            $('#email').val(data.email);
            $('#lstAreas').val(data.area);
            $('#lstEstatus').val(data.estatus);
            $('#hiddenTxtA').val(data.photo);
            $('#saveChanges').click(function(){
                Swal.fire({
                    title: 'Confirmación',
                    text: "¿Está seguro de querer modificar este administrador?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar'
                  }).then((result) => {
                    if (result.value) {
                        let idAdminLog = localStorage.getItem("idAdmin");
                        let idAdmin = data.idAdmin;
                        let photo = document.getElementById("hiddenTxtA").value;
                        let name = document.getElementById("name").value;
                        let surname = document.getElementById("surname").value;
                        let email = document.getElementById("email").value;
                        let area = $("#lstAreas :selected").text();
                        let estatus = $("#lstEstatus :selected").text();
                        let token = localStorage.getItem("token");
                        //Declare data json for ajax request.
                        let updateData = {
                            idAdmin:idAdmin,
                            idAdminLog:idAdminLog,
                            photo:photo,
                            name:name,
                            surname:surname,
                            email:email,
                            area:area,
                            estatus:estatus,
                            token:token
                        };
                        //Ajax request.
                        $.ajax({
                            type:"POST",
                            url: "/rest/admin/update",
                            async:true,
                            data:updateData
                        }).done(function(data){
                            var result = '';
                            result = JSON.parse(data);
                            //If there's an exception...
                            if(result.exception != null){
                                Swal.fire("La consulta no se pudo procesar",
                                result.exception,
                                "error");
                            }
                            //If not...
                            else{
                                Swal.fire("Registro modificado con éxito",
                                "",
                                "success");
                                cleanFields();
                            }
                        });
                    }
                });   
            });
        }
    });
}

function deleteAdmin(idAdmin) {
    //Get data for token validation.
    let token = localStorage.getItem("token");
    let idAdminLog = localStorage.getItem("idAdmin");
    //Declare data json for Ajax request.
    let data = {
        idAdmin:idAdmin,
        token:token,
        idAdminLog:idAdminLog
    }
    Swal.fire({
        title: 'Confirmación',
        text: "¿Está seguro de querer eliminar este administrador?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {
            //Ajax request.
            $.ajax({
                type:"POST",
                url: "/rest/admin/delete",
                async:true,
                data:data
            }).done(function(data){
                var result = '';
                result = JSON.parse(data);
                //If there's an exception...
                if(result.exception != null){
                    Swal.fire("La consulta no se pudo procesar",
                    result.exception,
                    "error");
                }else{
                    Swal.fire("Eliminación exitosa",
                    "",
                    "success");
                    $("#adminTable td").remove();
                    getAll();
                }
            });
        }
    });   
}

