//This import needed. Like logout function and set data for topbar.
import logout from '/js/logout.js';
import setData from '/js/mainPage.js';

//This function detects when any of the buttons on the add administrator page is pressed, then executes the corresponding function.
window.onload = function () {
    setData();
    let getPhoto = document.getElementById("inputPhotoButton");
    getPhoto.addEventListener("click", function(){
        $( "#inputPhoto" ).click();
    });
    let addButton = document.getElementById("addAdminButton");
    addButton.addEventListener("click", addAdmin);
    $('#inputPhoto').on("change", function(){ loadFoto(); });
    $("#logoutTag").on('click', logout);
}

//Function to add new administrators.
function addAdmin(){
    //Get the data values.
    let idAdmin = localStorage.getItem("idAdmin");
    let photo = document.getElementById("hiddenTxtA").value;
    let name = document.getElementById("name").value;
    let surname = document.getElementById("surname").value;
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    let area = $("#lstAreas :selected").text();
    let estatus = $("#lstEstatus :selected").text();
    let token = localStorage.getItem("token");

    //Declare data json for ajax request.
    let data = {
        idAdminLog:idAdmin,
        photo:photo,
        name:name,
        surname:surname,
        email:email,
        password,password,
        area:area,
        estatus:estatus,
        token:token
    };

    //This iteration validate if any value is empty or select element has an invalid value.
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            if(data[key] === "" || data[key] === "Tipo de área" || data[key] === "Estatus"){
                Swal.fire("La consulta no se pudo procesar",
                "Por favor, llene todos los campos",
                "error");
                return;
            }
        }
    }

    //Ajax request.
    $.ajax({
        method: "POST",
        url: "/rest/admin/addAdmin",
        data: data,
        async: true
    }).done(function (data) {
        var result = '';
        result = JSON.parse(data);
        //If there is any exception, this alert will show it.
        if (result.exception != null) {
            Swal.fire("La consulta no se pudo procesar",
            result.exception,
            "error");
        }
        //If not, this alert show a confirmation message.
        else {
            Swal.fire("Administrador registrado con exito",
            result.name,
            "success");
            clearFields();
        }
    });
}

//Function for load photo on img component, also save base64 of image on a hidden textfield.
function loadFoto(){
    var fileChooser=document.getElementById("inputPhoto");
    var foto = document.getElementById("userPhoto");
    var base64=document.getElementById("hiddenTxtA");
    //if fileChooser has a length > 0, means that have a photo!
    if (fileChooser.files.length > 0) {
        var fr = new FileReader();
        fr.onload = function(){
            foto.src="";
            foto.src=fr.result;
            base64.value="";
            base64.value=foto.src.replace(/^data:image\/(png|jpg|jpeg);base64,/, '');
        };
        fr.readAsDataURL(fileChooser.files[0]);
    }
}


export default function clearFields(){
    $("#imgFoto").prop('src', './media/imageicon2.png');
    $("#hiddenTxtA").val("");
    $("#name").val("");
    $("#surname").val("");
    $("#email").val("");
    $("#password").val("");
    $("#idAdmin").val("");
    $('#lstAreas').val("Tipo de área");
    $('#lstEstatus').val(1);
}