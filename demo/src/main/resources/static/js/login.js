//This function detects when any of the buttons on the login page is pressed, then executes the corresponding function.
window.onload = function () {
    let loginButton = document.getElementById("loginButton");
    loginButton.addEventListener("click", login);
}

//This function does the whole login process: Validation, Ajax request and jwt
function login(){
    let email = document.getElementById("txtEmail").value;
    let password = document.getElementById("txtPassword").value;
    //Check if values are not empty
    if(email === ""|| password === ""){
        Swal.fire("Campos Vacíos",
                "Por favor, llene todos los campos",
                "error");
    }else{
        data = {
            email: email,
            password:password
        }
        //Ajax request
        $.ajax({
            method: "POST",
            url: "/rest/log/login",
            data: data,
            async: true
        }).done(function (data) {
                var response = '';
                response = JSON.parse(data);
            if (response.exception != null) {
                Swal.fire("Error al iniciar sesión",
                    response.exception,
                    "error");
            }else if(response.token == ""){
                Swal.fire({
                    title: 'Sesión ya activa',
                    text: "Ya existe una sesión activa con este usuario, desea cerrarla e iniciar una nueva en este dispositivo?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar'
                  }).then((result) => {
                    if (result.value) {
                        let idAdmin = response.idAdmin;
                        let data = {idAdmin:idAdmin}
                        $.ajax({
                            method: "POST",
                            url: "/rest/logu/logout",
                            data: data,
                            async: true
                        }).done(function (data) {
                                var result = '';
                                result = JSON.parse(data);
                            if (response.exception != null) {
                                Swal.fire("Error en el sistema. Por favor contacte al administrador.",
                                    result.exception,
                                    "error");
                            }
                            else {
                                Swal.fire(
                                    'Sesión cerrada',
                                    'La sesión ha sido cerrada, puede iniciar una nueva en este dispositivo ahora.',
                                    'success'
                                  )
                            }
                        });
                      
                    }
                })
            }
            else {
                let tokenAdmin = response.token;
                let photo = response.photo;
                let name = response.name + " " + response.surname;
                let area = response.area;
                let idAdmin = response.idAdmin;
                Swal.fire("Bienvenido!",
                name,
                "success");
                setTimeout(function(){
                    localStorage.setItem("foto",photo)
                    localStorage.setItem("nombre", name);
                    localStorage.setItem("area", area);
                    localStorage.setItem("token", tokenAdmin);
                    localStorage.setItem("idAdmin", idAdmin);
                    window.location.href = "mainPage.html";
                                        }, 2000);
            }
        });
    }
}