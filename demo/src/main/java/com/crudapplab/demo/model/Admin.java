package com.crudapplab.demo.model;

public class Admin {
    int idAdmin;
    String photo;
    String name;
    String surname;
    String area;
    String estatus;
    String email;
    String password;
    String token;

    public Admin(){};

    public Admin(int idAdmin, String photo, String name, String surname, String area, String estatus, String email, String password, String token) {
        this.idAdmin = idAdmin;
        this.photo = photo;
        this.name = name;
        this.surname = surname;
        this.area = area;
        this.estatus = estatus;
        this.email = email;
        this.password = password;
        this.token = token;
    }

    public int getIdAdmin() {
        return this.idAdmin;
    }

    public void setIdAdmin(int idAdmin) {
        this.idAdmin = idAdmin;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getArea() {
        return this.area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEstatus() {
        return this.estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
