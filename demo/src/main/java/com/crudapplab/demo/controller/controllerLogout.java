package com.crudapplab.demo.controller;

import com.crudapplab.demo.database.conexionMySQL;
import java.sql.CallableStatement;

public class controllerLogout {
    public String deleteTokenAdmin(int idAdmin) throws Exception{
        String query="{CALL deleteAdminToken(?)}";
        conexionMySQL connMySQL = new conexionMySQL();
        java.sql.Connection conn = connMySQL.open();
        CallableStatement cstmt = conn.prepareCall(query);
        String out = "";
        
        try{
            cstmt.setInt(1, idAdmin);
            cstmt.execute();
            out="OK";
        }catch(Exception e){
            e.printStackTrace();
            out = e.toString();
        }
        cstmt.close();
        connMySQL.close();
        return out;
    }


}
