package com.crudapplab.demo.controller;
import java.sql.ResultSet;
import java.util.Date;

import javax.json.Json;

import java.sql.CallableStatement;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import com.crudapplab.demo.database.conexionMySQL;
import com.crudapplab.demo.model.Admin;




public class controllerLogin {
    public Admin logAdmin(String user, String password) throws Exception{
        Admin a = new Admin();
        String query="SELECT idAdmin, token, foto, nombre, apellido, area FROM administrador WHERE correo='"+user+"' && contrasenia='"+password+"';";
        conexionMySQL connMySQL = new conexionMySQL();
        java.sql.Connection conn = connMySQL.open();
        java.sql.Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next() == true){
            a.setPhoto(rs.getString("foto"));
            a.setName(rs.getString("nombre"));
            a.setSurname(rs.getString("apellido"));
            a.setIdAdmin(rs.getInt("idAdmin"));
            a.setArea(rs.getString("area"));
            a.setToken(rs.getString("token"));
            if(a.getToken().isEmpty())
            {
                long time = System.currentTimeMillis();
                String key = "secretkey";
                try{
                    String jwt = Jwts.builder()
                                .signWith(SignatureAlgorithm.HS256, key)
                                .setSubject(a.getName())
                                .setIssuedAt(new Date(time))
                                .setExpiration(new Date(time+3600000))
                                .claim("email", user)
                                .compact();
                javax.json.JsonObject json = Json.createObjectBuilder()
                                        .add("JWT", jwt).build();
                a.setToken(json.getString("JWT"));
                }catch(Exception e){
                    e.printStackTrace();
                }
                tokenAdmin(a);
            }
            else
            {
                a.setToken("");
            }
        }
        rs.close();
        stmt.close();
        connMySQL.close();
        conn.close();
        return a;
    }

    public int tokenAdmin(Admin a) throws Exception{
        String query2="{CALL updateAdminToken(?,?)}";
        conexionMySQL connMySQL = new conexionMySQL();
        java.sql.Connection conn = connMySQL.open();
        CallableStatement cstmt = conn.prepareCall(query2);
        
        cstmt.setInt(1, a.getIdAdmin());
        cstmt.setString(2, a.getToken());
        cstmt.execute();
        
        cstmt.close();
        connMySQL.close();
        
        return a.getIdAdmin();
    }

    public String getTokenAdmin(int idAdmin) throws Exception{
        String dbToken = "";
        String query="SELECT token FROM administrador WHERE idAdmin = '"+idAdmin+"'";
        conexionMySQL connMySQL = new conexionMySQL();
        java.sql.Connection conn = connMySQL.open();
        java.sql.Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next() == true){
            dbToken = rs.getString("token");
        }
        rs.close();
        stmt.close();
        connMySQL.close();
        conn.close();
        return dbToken;
    }
}
