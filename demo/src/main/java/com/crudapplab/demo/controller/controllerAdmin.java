package com.crudapplab.demo.controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.crudapplab.demo.database.conexionMySQL;
import com.crudapplab.demo.model.Admin;

public class controllerAdmin {
    public int addAdmin(Admin adm) throws Exception{
        //Generate the call to the procedure
        String query="{CALL insertAdmin(?,?,?,?,?,?,?," +     //Admin data
                                                   "?)}";    //Return values
        //Generate the connection and open it
        conexionMySQL connMySQL = new conexionMySQL();
        Connection conn = connMySQL.open();
        //Generate the statement for query
        CallableStatement cstmt = conn.prepareCall(query);
        //Fill Admin data according to the procedure parameters.
        cstmt.setString(1, adm.getPhoto());
        cstmt.setString(2, adm.getName());
        cstmt.setString(3, adm.getSurname());
        cstmt.setString(4, adm.getArea());
        cstmt.setString(5, adm.getEstatus());
        cstmt.setString(6, adm.getEmail());
        cstmt.setString(7, adm.getPassword());
        //Register the output parameters.
        cstmt.registerOutParameter(8, Types.INTEGER);
        //Execute the procedure.
        cstmt.execute();
        //Retrieve the generated id
        adm.setIdAdmin(cstmt.getInt(8));
        //Close objects
        cstmt.close();
        connMySQL.close();
        //return new id
        return adm.getIdAdmin();
    }

    public void update(Admin adm) throws Exception{
        //Generate the call to the procedure
        String query="{CALL updateAdmin(?,?,?,?,?,?,?)}";    //Admin data
        //Generate the connection and open it
        conexionMySQL connMySQL = new conexionMySQL();
        Connection conn = connMySQL.open();
        //Generate the statement for query
        CallableStatement cstmt = conn.prepareCall(query);
        //Fill Admin data according to the procedure parameters.
        cstmt.setInt(1, adm.getIdAdmin());
        cstmt.setString(2, adm.getPhoto());
        cstmt.setString(3, adm.getName());
        cstmt.setString(4, adm.getSurname());
        cstmt.setString(5, adm.getArea());
        cstmt.setString(6, adm.getEstatus());
        cstmt.setString(7, adm.getEmail());
        //Execute the procedure.
        cstmt.execute();
        //Close objects
        cstmt.close();
        connMySQL.close();
    }

    public List<Admin> getAll() throws Exception{
        //Declare SQL query.
        String query = "SELECT * FROM administrador";
        //Generate db object and start conexion.
        conexionMySQL connMySQL = new conexionMySQL();
        Connection conn = connMySQL.open();
        //3.- Generar el statement que lleva la consulta
        PreparedStatement pstmt = conn.prepareStatement(query);
        //4.- Ejecutar y almacenar el resultado
        ResultSet rs = pstmt.executeQuery();
        //5.- Generar la estructura que contendrá toda la información
        List<Admin> adminList = new ArrayList<>();

        while(rs.next()){
            Admin adm = new Admin();
            adm.setIdAdmin(rs.getInt("idAdmin"));
            adm.setPhoto(rs.getString("foto"));
            adm.setName(rs.getString("nombre"));
            adm.setSurname(rs.getString("apellido"));
            adm.setArea(rs.getString("area"));
            adm.setEmail(rs.getString("correo"));
            adm.setEstatus(rs.getString("estatus"));
            adminList.add(adm);
        }

        return adminList;
    }

    public List<Admin> search(String name, String filterArea, String filterEstatus) throws Exception{
        //Declare SQL query.
        String query = "";
        //Check if there's any filter on search and build SQL query.
        if(filterArea.isEmpty() && filterEstatus.isEmpty()){
            query = "SELECT * FROM administrador WHERE nombre LIKE "+"\""+name+"\";";
        }else if(filterArea.isEmpty()){
            query = "SELECT * FROM administrador WHERE nombre LIKE "+"\""+name+"\""+" AND estatus = "+"\""+filterEstatus+"\";";
        }else if(filterEstatus.isEmpty()){
            query = "SELECT * FROM administrador WHERE nombre LIKE " +"\""+name+"\""+" AND area = "+"\""+filterArea+"\";";
        }else{
            query = "SELECT * FROM administrador WHERE nombre LIKE " +"\""+name+"\"" +" AND area = "+"\""+filterArea+"\""+" AND estatus = "+"\""+filterEstatus+"\""+";";
        }
        System.out.println(query);
        //Generate db object and start conexion.
        conexionMySQL connMySQL = new conexionMySQL();
        Connection conn = connMySQL.open();
        //3.- Generar el statement que lleva la consulta
        PreparedStatement pstmt = conn.prepareStatement(query);
        //4.- Ejecutar y almacenar el resultado
        ResultSet rs = pstmt.executeQuery();
        //5.- Generar la estructura que contendrá toda la información
        List<Admin> adminList = new ArrayList<>();
        while(rs.next()){
            Admin adm = new Admin();
            adm.setIdAdmin(rs.getInt("idAdmin"));
            adm.setPhoto(rs.getString("foto"));
            adm.setName(rs.getString("nombre"));
            adm.setSurname(rs.getString("apellido"));
            adm.setArea(rs.getString("area"));
            adm.setEmail(rs.getString("correo"));
            adm.setEstatus(rs.getString("estatus"));
            adminList.add(adm);
        }
        return adminList;
    }

    public void delete(int idAdmin) throws Exception {
        //Declare SQL query.
        String query = "UPDATE administrador SET estatus='Inactivo' WHERE idAdmin=" + idAdmin;
        //Generate db object and start conexion.
        conexionMySQL connMySQL = new conexionMySQL();
        Connection conn = connMySQL.open();
        //3.- Generar un statement que lleve las sentencias
        Statement stmt = conn.createStatement();
        //4.- Se envía la sentencia sql(consulta)
        stmt.executeUpdate(query);
        //5.- Se cierra el statement
        stmt.close();
        //6.- Cierre de conexión
        connMySQL.close();
        return;
    }
}
