package com.crudapplab.demo.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;

import com.crudapplab.demo.controller.controllerAdmin;
import com.crudapplab.demo.controller.controllerLogin;
import com.crudapplab.demo.model.Admin;
import com.google.gson.Gson;

@RestController
@RequestMapping("/rest/admin")
public class adminREST {
    @PostMapping("/addAdmin")
    @ResponseBody
    public String addAdmin(@FormParam("idAdminLog") Integer idAdminLog,
                           @FormParam("photo") @DefaultValue("null") String photo,
                           @FormParam("name") @DefaultValue("null") String name,
                           @FormParam("surname") @DefaultValue("null") String surname,
                           @FormParam("email") @DefaultValue("null") String email,
                           @FormParam("password") @DefaultValue("null") String password,
                           @FormParam("area") @DefaultValue("null") String area,
                           @FormParam("estatus") @DefaultValue("null") String estatus,
                           @FormParam("token") @DefaultValue("null") String token) throws Exception {
        String out = "";
        Admin adm = new Admin(0,photo,name,surname,area,estatus,email,password,"");
        controllerLogin cl = new controllerLogin();
        controllerAdmin ca = new controllerAdmin();

        if(!cl.getTokenAdmin(idAdminLog).equals(token)){
            out="{\"exception\": \"Usted no tiene acceso a esta funcionalidad (Token error)\"}";
        }else{
            try {
                int newId = ca.addAdmin(adm);
                if(newId != 0){
                    out="{\"response\": \"Usuario Registrado\"}";
                }else{
                    out="{\"exception\": \"Usuario no agregado, verifique los datos ingresados\"}";
                }
            } catch (Exception e) {
                e.printStackTrace();
                out = "{\"exception\":\" "+e.toString()+" \"}";
            }
        }
        return out;
    }

    @PostMapping("/update")
    @ResponseBody
    public String update(@FormParam("idAdmin") Integer idAdmin,
                         @FormParam("idAdminLog") Integer idAdminLog,
                         @FormParam("photo") @DefaultValue("null") String photo,
                         @FormParam("name") @DefaultValue("null") String name,
                         @FormParam("surname") @DefaultValue("null") String surname,
                         @FormParam("email") @DefaultValue("null") String email,
                         @FormParam("area") @DefaultValue("null") String area,
                         @FormParam("estatus") @DefaultValue("null") String estatus,
                         @FormParam("token") @DefaultValue("null") String token) throws Exception {
        String out = "";
        Admin adm = new Admin(idAdmin,photo,name,surname,area,estatus,email,"","");
        controllerLogin cl = new controllerLogin();
        controllerAdmin ca = new controllerAdmin();

        if(!cl.getTokenAdmin(idAdminLog).equals(token)){
            out="{\"exception\": \"Usted no tiene acceso a esta funcionalidad (Token error)\"}";
        }else{
            try {
                ca.update(adm);
                out="{\"response\": \"Usuario Actualizado\"}";
            } catch (Exception e) {
                e.printStackTrace();
                out = "{\"exception\":\" "+e.toString()+" \"}";
            }
        }
        return out;
    }

    @GetMapping("/getAll")
    @ResponseBody
    public String getAll(@QueryParam("token") @DefaultValue("") String token,
                         @QueryParam("idAdminLog") @DefaultValue("0") Integer idAdminLog) throws Exception{
        
        String out="";
        controllerLogin cl = new controllerLogin();
        controllerAdmin ca = new controllerAdmin();

        if(!cl.getTokenAdmin(idAdminLog).equals(token)){
            out="{\"exception\": \"Usted no tiene acceso a esta funcionalidad (Token error)\"}";
        }else{
            try{
                List<Admin> admin = new ArrayList<>();
                admin=ca.getAll();
                if(!admin.isEmpty())
                    out=new Gson().toJson(admin);
                else{
                    out="{\"exception\":\"No data\"}";
                }
            }
            catch(Exception e){
                e.printStackTrace();
                out="{\"exception\":\""+e.toString()+"\"}";
            }
        }
        return out;
    }

    @PostMapping("/delete")
    @ResponseBody
    public String delete(@FormParam("idAdmin") @DefaultValue("") Integer idAdmin,
                         @FormParam("token") @DefaultValue("") String token,
                         @FormParam("idAdminLog") @DefaultValue("0") Integer idAdminLog) throws Exception{
        
        String out="";
        controllerLogin cl = new controllerLogin();
        controllerAdmin ca = new controllerAdmin();

        if(!cl.getTokenAdmin(idAdminLog).equals(token)){
            out="{\"exception\": \"Usted no tiene acceso a esta funcionalidad (Token error)\"}";
        }else{
            try{
                ca.delete(idAdmin);
                out="{\"response\":\"Usuario Eliminado\"}";
            }
            catch(Exception e){
                e.printStackTrace();
                out="{\"exception\":\""+e.toString()+"\"}";
            }
        }
        return out;
    }

    @GetMapping("/search")
    @ResponseBody
    public String search(@QueryParam("idAdminLog") @DefaultValue("0") Integer idAdminLog,
                         @QueryParam("token") @DefaultValue("") String token,
                         @QueryParam("name") @DefaultValue("") String name,
                         @QueryParam("filterArea") @DefaultValue("") String filterArea,
                         @QueryParam("filterEstatus") @DefaultValue("") String filterEstatus) throws Exception{
        String out="";
        controllerLogin cl = new controllerLogin();
        controllerAdmin ca = new controllerAdmin();
        if(!cl.getTokenAdmin(idAdminLog).equals(token)){
            out="{\"exception\": \"Usted no tiene acceso a esta funcionalidad (Token error)\"}";
        }else{
            try{
                List<Admin> adminList = new ArrayList<>();
                adminList = ca.search(name, filterArea, filterEstatus);
                if(!adminList.isEmpty())
                    out=new Gson().toJson(adminList);
                else{
                    out="{\"exception\":\"No data\"}";
                }
            }catch(Exception e){
                e.printStackTrace();
                out="{\"exception\":\""+e.toString()+"\"}";
            }
        }
        return out;
    }
}
