package com.crudapplab.demo.rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import com.google.gson.Gson;
import com.crudapplab.demo.controller.controllerLogin;
import com.crudapplab.demo.model.Admin;

@RestController
@RequestMapping("/rest/log")
public class loginREST {
    @PostMapping("/login")
    @ResponseBody
    public String login(@FormParam("email") @DefaultValue("null") String email,
                        @FormParam("password") @DefaultValue("null") String password) {
        String out = "";
        Admin ad = new Admin();
        controllerLogin cl = new controllerLogin();
        try {
            ad = cl.logAdmin(email,password);
            if(ad.getToken() == ""){
                out= new Gson().toJson(ad);
            }
            else if(ad.getIdAdmin() != 0){
                out= new Gson().toJson(ad);
            }else{
                out="{\"exception\": \"Usuario/contraseña incorrectos\"}";
            }
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"exception\":\" "+e.toString()+" \"}";
        }
        return out;
    }
}
