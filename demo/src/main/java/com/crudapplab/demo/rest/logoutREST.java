package com.crudapplab.demo.rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.ws.rs.FormParam;
import com.crudapplab.demo.controller.controllerLogout;

@RestController
@RequestMapping("/rest/logu")
public class logoutREST {
    @PostMapping("/logout")
    @ResponseBody
    public String login(@FormParam("idAdmin") int idAdmin) {
        String out = "";
        String response = "";
        controllerLogout cl = new controllerLogout();
        try {
            response = cl.deleteTokenAdmin(idAdmin);

            if(response != "OK"){
                out="{\"exception\":\" "+response+" \"}";
            }else{
                out="{\"response\": \"OK\"}";
            }
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"exception\":\" "+e.toString()+" \"}";
        }
        return out;
    }
}
